//# Hoofdstuk 2: Core JavaScript
//# Opdracht 23.1

class Person {
    constructor(firstName = "", lastName = "") {
        this._firstName = firstName;
        this._lastName = lastName;
    }

    get firstName() {
        return this._firstName;
    }

    get lastName() {
        return this._lastName
    }

    toString() {
        return `Person [firstName=${this._firstName},lastName=${this._lastName}]`;
    }
}

class Student extends Person {
    constructor(firstName, lastName, studentId = 0) {
        super(firstName, lastName);
        this._studentId = studentId;
    }

    get studentId() {
        return this._studentId;
    }

    study() {
        addLine("Studying")
    }

    toString() {
        return `Student [firstName=${this._firstName},lastName=${this._lastName},studentId=${this._studentId}]`;
    }

}

class Teacher extends Person {
    constructor(firstName, lastName, employeeId = 0) {
        super(firstName, lastName);
        this._employeeId = employeeId;
    }

    get empoyeeId() {
        return this._employeeId;
    }

    teach() {
        addLine("Teaching");
    }

    toString() {
        return `Teacher [firstName=${this._firstName},lastName=${this._lastName},employeeId=${this._employeeId}]`;
    }

}

//add a code block for repeatable code use
function o23() {
    const person = new Person("Homer", "Simpson");
    addLine(person.toString());

    const student = new Student("Bart", "Simpson", 1);
    addLine(student.toString());
    student.study();

    const teacher = new Teacher("Marge", "Simpson", 200);
    addLine(teacher.toString());
    teacher.teach();
}