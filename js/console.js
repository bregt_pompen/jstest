var con = document.getElementById("myConsole");

function printTitle(title, useTags = true, char = "="){
    if(title && typeof title === "string" && title.length>0){
        if(useTags == false){
            addLine(title);
            let underline = "";
            for(let i=0; i<title.length; i++){
                underline += char;
            }
            addLine(underline);
        }else{
            addLine("<u><b>" + title + "</u></b>");
        }
    }
}

function addLine(text) {
    console.log(text);
    if (con.innerHTML.length > 0) {
        text = "<br>" + text;
    }
    con.innerHTML += text;
}

function addLine(...args) {
    console.log(...args);
    var text = con.innerHTML.length > 0?"<br>":""
    for (arg of args) {
        text += new String(arg);
    }
    con.innerHTML += text;
}

function clearConsole() {
    con.innerHTML = "";
}