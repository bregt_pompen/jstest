const area = document.getElementById("area");
const popup = document.getElementById("popup");

function showPopup(event) {
    popup.style.top = event.pageY + "px";
    popup.style.left = event.pageX + "px";
    popup.style.visibility = "visible";

}

function bgcolorbutton(event) {

    var x = event.keyCode;
    if (x == 82) {
        area.style.backgroundColor = "red";
    }
    if (x == 89) {
        area.style.backgroundColor = "yellow";
    }
    if (x == 66) {
        area.style.backgroundColor = "blue";
    }

    if (x == 71) {
        area.style.backgroundColor = "green";
    }

}
document.addEventListener("keydown", bgcolorbutton);
area.addEventListener("mouseup", showPopup);