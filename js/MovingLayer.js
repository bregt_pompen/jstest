let x = 300;
let y = 300;

let timer;

//Animation f

function animation() {
    const layer = document.getElementById("layer");
    if ((--x == 0) || (--y == 0)) {

        stopAnimation();
    }
    layer.style.left = x + "px";
    layer.style.top = y + "px";
}

function startAnimation() {
    const layer = document.getElementById("layer");

    layer.style.visibility = "visible";
    layer.style.left = x + "px";
    layer.style.top = y + "px";

    timer = setInterval(animation, 20);

}

function stopAnimation() {
    clearInterval(timer);
}

function ani() {
    setTimeout(startAnimation, 5000);
}
// move element with mouse
var mousePosition;
var offset = [0, 0];
var div;
var isDown = false;

div = document.createElement("div");
div.style.position = "absolute";
div.style.left = "0px";
div.style.top = "0px";
div.style.width = "100px";
div.style.height = "100px";
div.style.background = "red";
div.style.color = "blue";
var textnode = document.createTextNode("DRAG ME");



document.getElementById("boddiv").appendChild(div);
div.appendChild(textnode);

div

div.addEventListener('mousedown', function(e) {
    isDown = true;
    offset = [
        div.offsetLeft - e.clientX,
        div.offsetTop - e.clientY
    ];
}, true);

document.addEventListener('mouseup', function() {
    isDown = false;
}, true);

document.addEventListener('mousemove', function(event) {
    event.preventDefault();
    if (isDown) {
        mousePosition = {

            x: event.clientX,
            y: event.clientY

        };
        div.style.left = (mousePosition.x + offset[0]) + 'px';
        div.style.top = (mousePosition.y + offset[1]) + 'px';
    }
}, true);

window.addEventListener("load", ani);