function bubsAndCaps() {
    const area1 = document.getElementById("zone1");
    const area2 = document.getElementById("zone2");
    document.addEventListener("click", docClickCap, true);
    document.addEventListener("click", docClickBub, false);
    area1.addEventListener("click", area1ClickCap, true);
    area1.addEventListener("click", area1ClickBub, false);
    area2.addEventListener("click", area2ClickCap, true);
    area2.addEventListener("click", area2ClickBub, false);
}

function docClickCap() {
    console.log("Capture Document");
}

function docClickBub() {
    console.log("Bubble Document");
}

function area1ClickCap() {
    console.log("Capture Area1");
}

function area1ClickBub() {
    console.log("Bubble Area1");
}

function area2ClickCap() {
    console.log("Capture Area2");
}

function area2ClickBub() {
    console.log("Bubble Area2");
}

window.addEventListener("load", bubsAndCaps);