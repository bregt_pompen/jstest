function validate(e) {

    let valid = true;
    const form = document.getElementById("myForm");
    const inv = document.getElementById("inv");
    const inv2 = document.getElementById("inv2");
    const inv3 = document.getElementById("inv3");
    const inv4 = document.getElementById("inv4");
    const firstNameField = document.getElementById("firstName");
    const lastNameField = document.getElementById("lastName");
    const emailField = document.getElementById("emailadress");
    const birthdayField = document.getElementById("birthday");
    firstNameField.removeAttribute("class");




    let dateRegex = /^[0-3]?[0-9][./-][0-1]?[0-9][./-]\d{4}$/;
    let emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+[\.])+([a-zA-Z0-9]{2,4})+$/;



    if (lastNameField.value.length == 0) {
        valid = false;
        inv.style.visibility = "visible";
        lastNameField.setAttribute("class", "error");
    } else {
        lastNameField.removeAttribute("class");
        inv.style.visibility = "hidden";
    }

    if (firstNameField.value.length == 0) {
        valid = false;
        inv2.style.visibility = "visible";
        firstNameField.setAttribute("class", "error");
    } else {
        firstNameField.removeAttribute("class");
        inv2.style.visibility = "hidden";
    }




    if (!emailRegex.test(emailField.value)) {
        valid = false;
        inv3.style.visibility = "visible";
        emailField.setAttribute("class", "error");
    } else {
        emailField.removeAttribute("class");
        inv3.style.visibility = "hidden";
    }

    if (!dateRegex.test(birthdayField.value)) {
        valid = false;
        inv4.style.visibility = "visible";
        birthdayField.setAttribute("class", "error");
    } else {
        birthdayField.removeAttribute("class");
        inv4.style.visibility = "hidden";
    }



    console.log("valids + " + valid);
    if (!valid) {

        const errorElement = document.getElementById("error");
        errorElement.innerHTML = "Some user input was invalid!";

        e.preventDefault();
    }

    saveFormCookie(form);



}


function errormsg() {

    const form = document.getElementById("myForm");
    if (form != null) {
        form.addEventListener("submit", validate);
        loadFormCookie(form);
    }
}

function getParameter(name) {
    const queryString = decodeURIComponent(location.search.replace(/\+/g, "%20"));
    const regex = new RegExp(name + "=([^&]+)");
    const result = regex.exec(queryString);

    if (result) { return result[1]; } else { return null; }
}

function page2result() {
    const fName = getParameter("firstName");
    const lName = getParameter("lastName");
    try {
        document.getElementById("resultaat").innerHTML = "Hallo " + fName + " " + lName; //ment for result page will give error
    } catch (error) {

    }
}

window.addEventListener("load", page2result);

window.addEventListener("load", errormsg);