var bgWhite = false;

function testFunc() {
    addLine("Test!!");
}

function swapColors() {
    bgWhite = !bgWhite;
    if (bgWhite) {
        con.style.backgroundColor = "white";
        con.style.color = "black";
        addLine("Black on white");

    } else {
        con.style.backgroundColor = "black";
        con.style.color = "white";
        addLine("white on black");
    }
}

function opdracht1() {
    let name = prompt("enter you name please:");
    addLine(`Hello ${name}`);
    let age = prompt("enter your age please:");
    //addLine(`You are ${age} old`);
    addLine(`You are ${age} years old`);
    let gehuwd;
    do {
        gehuwd = prompt("are you married? type 'maried' or 'unmarried'");
    } while (gehuwd != "married" && gehuwd != "unmarried");
    //addLine(`Hello ${gehuwd}`);
    addLine(`You are ${gehuwd}`);
}

function opdracht2() {
    let age = prompt("enter your age please:");
    let days = age * 365;
    let weeks = days / 7;
    let adult = age >= 18 ? "adult" : "not adult";


    addLine(`You are ${days} days old`);
    addLine(`You are ${Math.floor(weeks)} weeks old`);
    addLine(`You are ${adult}`);
}

function opdracht3() {
    let a = 100
    while (a <= 120) {
        ++a;
        addLine(a);
    }
}

function opdracht32() {
    let age;
    do {
        age = prompt("enter your age please:");
    } while (age > 150 || age < 0);
    addLine(`You are ${age} years old`);
    addLine("(age = correct)");
}

function opdracht4() {
    for (let i = 120; i >= 100; i--) { addLine(i); }
}

function opdracht5() {
    let weight = prompt("enter your weight in kilos");
    let length = prompt("enter your length in centimeters");
    let bmi = weight / (length * length);

    if (bmi < 18) addLine(`your bmi is${bmi} so you'r an underweight!`);
    else if (bmi < 26) addLine(`your bmi is ${bmi} so you'r weight is normal!`);
    else if (bmi < 28) addLine(`your bmi is ${bmi} so you'r a light overweight!`);
    else if (bmi < 31) addLine(`your bmi is ${bmi} so you'r an underweight!`);
    else if (bmi < 41) addLine(`your bmi is ${bmi} so you'r an heavy underweight!`);
    else addLine(`your bmi is${bmi} so you'r an underweight!`);
}

function opdracht6() {
    let langue = prompt("enter your langue 'nl/fr/en'");
    switch (langue) {
        case "nl":
            addLine("Dag wereld");
            break;
        case "fr":
            addLine("Bonjour le monde");
            break;
        case "en":
            addLine("Hello World");
            break;
        default:
            addLine("Your input is invalid/Je hebt een verkeerde input gegeven/Votre input est incorrect");
            break;
    }

}

function opdracht8() {
    function factorial(v) {
        if (v <= 1) {
            return 1;
        } else {
            return factorial(v - 1) * v;
        }
    }
    let result = factorial(5);
    addLine(result);
}

function opdracht9() {

    function factorial(v) {
        if (v <= 1) {
            return 1;
        } else {
            return factorial(v - 1) * v;
        }
    }

    function specialSum(value1, value2, operation) {
        return operation(value1) + operation(value2);
    }

    function printCalculation() {
        let num1 = 5;
        let num2 = 4;
        let output = specialSum(num1, num2, factorial);
        addLine(`sum of factorial callculation of ${num1} and ${num2} = ${output}`);
    }
    printCalculation(5); //example with 5
}

function opdracht10() {
    function createIncrementorWithParam(step = 1) {
        return v => v + step;
    }

    function createIncrementorNoParam(step = 1) {
        let v = 0;
        return () => v += step;
    }

    let inc1 = createIncrementorWithParam();
    let inc2 = createIncrementorWithParam(2);

    addLine("** step=1 With Param**");
    for (let i = 0; i <= 4; i++) {
        addLine(inc1(-i));
    }

    addLine("** step=2 With Param**");
    for (let i = 0; i <= 4; i = inc2(i))
        addLine((i));

    addLine("****");
    let inc3 = createIncrementorNoParam();
    let inc4 = createIncrementorNoParam(2);

    addLine()("** step=1 No Param**");
    for (let i = 0; i <= 4; i++) {
        addLine(inc3());
    }

    addLine()("** step=2 No Param**");
    for (let i = 0; i <= 4; i = inc4())
        addLine((i));
}

function opdracht11() {
    let v1;
    let v2;
    do {
        v1 = prompt("enter value you want to parse");
        if (isNaN(v1)) { break; }
        v2 = prompt("enter another value you want to parse");
        if (isNaN(v2)) { break; }
    } while (!isFinite(v1) || !isFinite(v2));
    addLine("the sum of the 2 is " + v1 + v2);

}
//OPDRACHT12 WAS ZOEKEN
function opdracht13() {
    let myString = prompt("Enter some text:");
    addLine("Length of myString:", myString.length);
    addLine("myString to upper cases:", myString.toUpperCase());
    addLine("myString to lower cases:", myString.toLowerCase());

}

function opdracht132() {
    text = prompt("enter your emailadress");
    let text2 = new String(text);
    let split = text2.split("@");
    if (split.length == 2) { addLine("this is your emailadress? " + text2) } else {
        addLine("this is not an emailadress");
    }

}

function opdracht14() {
    let input = prompt("Enter a sentence:");
    let words = input.split(" ");
    for (word of words) {
        addLine(word);
    }
    addLine(words.length)

    addLine("** sort **")
    words.sort();
    for (word of words) {
        addLine(word);
    }

    addLine("** sorted by length **")
    words.sort((a, b) => a.length - b.length);
    for (word of words) {
        addLine(word);
    }

    addLine("** reverse **")
    words = words.reverse();
    for (word of words) {
        addLine(word);
    }

    addLine("** added first and last **")
    words.unshift("first");
    words.push("last");
    for (word of words) {
        addLine(word);
    }
}

function opdracht15() {
    let list = new Array(5);
    for (let i = 0; i < 5; i++)
        list[i] = new Array(5);

    // set each element of our 2dimentional array
    for (let i = 0; i < 5; i++) {
        for (let j = 0; j < 5; j++) {
            list[i][j] = i + j;
        }
    }

    // print each element and calulate the total sum
    let sum = 0;
    for (row of list) {
        for (el of row) {
            sum += el;
            //addLine(el);
        }
        addLine(row);
    }
    addLine("total = " + sum);
}

function opdracht16() {
    function average() {
        var length = arguments.length;
        var sum = 0;

        for (var i = 0; i < length; i++) {
            sum = sum + arguments[i];
        }

        return sum / length;
    }

    addLine(average(1, 2, 3, 4, 5));
}

function opdracht17() {
    var date = new Date();
    addLine(date.toLocaleString());

    var time = date.getHours();

    if (time < 12) {
        addLine("Good morning!");
    }
    if (time >= 12 && time < 18) {
        addLine("Good afternoon!");
    }
    if (time >= 18) {
        addLine("Good evening!");
    }
}

function opdracht172() {
    let birthday = prompt("Enter your date of birth:\n(YYYY-MM-DD)");
    let birth = new Date(birthday);
    let dow = birth.getDay();

    /*
	let day;
	switch(dow){
    		}
	// */
    let daysInAWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let day = daysInAWeek[dow];
    addLine("Day of the week: " + day);
}

function opdracht18() {
    var lotto = Array.from({ length: 6 }, () => Math.floor(Math.random() * 41) + 1);
    lotto.sort;
    addLine("lottonummers: " + lotto);
}

function opdracht19() {
    let mand = new Map();
    mand.set("schoes", 60.10);
    mand.set("pants", 34.80);
    mand.set("socks", 2.50);

    for (let key of mand.keys()) { addLine(key + " : " + mand.get(key)) }
    addLine("number of items : " + mand.size);
    mand.delete("pants");
    let total = 0;
    mand.forEach((value) => total += value)
    addLine("total :" + total)

}

function opdracht20() {
    let names = new Set();
    do {
        let newName = prompt(`Enter unique name nr ${names.size+1}:`, "");
        if (newName == null) {
            addLine("Cancel pressed!");
            break;
        } else if (newName.trim() === "") {
            alert("No empty or only spaces please!")
        } else {
            newName = newName.trim();
            if (names.has(newName)) {
                alert("That name is not unique!");
            } else {
                names.add(newName);
            }
        }
    } while (names.size < 5);

    names.forEach((v) => addLine(v));
    /*
    for(name of names){
    	addLine(name);
    }
    */

}

function opdracht21() {
    let bday = prompt("What's your birthday?");

    let dateRegex = /^[0-3]?[0-9][./-][0-1]?[0-9][./-]\d{4}$/;
    if (bday.match(dateRegex)) { addLine("You'r birthday is correctly entered : " + bday); } else addLine("This isn't a correctly entered date");
}

function opdracht212() {
    let email = prompt("What's your emailadress?");
    let emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (email.match(emailRegex)) { addLine("Your emailadress is correctly entered : " + email); } else addLine("this isn't a correctly entered emailadress");
}

function opdracht22() {
    o22();
}

function opdracht23() {

    o23();
}

function opdracht304() {
    addLine("appname  : " + navigator.appName);
    addLine("appversion : " + navigator.appVersion);
    addLine("platform : " + navigator.platform);
    if (navigator.onLine) { addLine(" Browser online"); } else addLine(" Browser ofline");


    if (navigator.cookieEnabled) {
        addLine("Cookies enabled");
        navigator.assign("../page2.html");
    } else addLine("Cookies disabled");
}

function opdracht305() {
    addLine("Page loaded or page size edited! Exercise : Chapter 3");
    addLine("screen height: " + screen.height);
    addLine("screen width: " +
        screen.width);
}


function opdracht306() { //+307+308
    const dateNode = document.getElementById("date");
    const today = new Date();
    var time = today.getHours();
    const sunormoon = document.getElementById("img");
    sunormoon.style.visibility = "visible";
    addLine("look start of this page for the time and greeting")
    if (time < 12) {
        sunormoon.setAttribute("src", "../img/sun.jpg");
        dateNode.style.backgroundColor = "lightblue";
        dateNode.innerHTML = "<p>Good Morning!</p><strong>" + today.toLocaleString() + "</strong>";
    } else if (time >= 12 && time < 18) {
        sunormoon.setAttribute("src", "../img/sun.jpg");
        dateNode.style.backgroundColor = "yellow";
        dateNode.innerHTML = "<p>Good afternoon!</p><strong>" + today.toLocaleString() + "</strong>";
    } else if (time >= 18 && time < 23) {
        sunormoon.setAttribute("src", "../img/moon.jpg");
        dateNode.style.backgroundColor = "blue";
        dateNode.innerHTML = "<p>Good Evening!</p><strong>" + today.toLocaleString() + "</strong>";
    } else if (time >= 23) {
        sunormoon.setAttribute("src", "../img/moon.jpg");
        dateNode.style.backgroundColor = "darkblue";
        dateNode.style.color = "white";
        dateNode.innerHTML = "<p>Good Night!</p><strong>" + today.toLocaleString() + "</strong>";
    }
}


let dateNode1 = document.getElementById("date");


function opdracht315() {
    addLine("Check above for time , hello world coming in console soon!");
    setTimeout("addLine('hello world')", 5000);


    var timePs = document.querySelector('#date2');

    setInterval(() => {
        function showtime() {
            const d = new Date();
            timePs.innerHTML = d.toLocaleTimeString();
        }
        showtime();
    }, 1000);

}


window.addEventListener("load", opdracht305);
window.addEventListener("resize", opdracht305);