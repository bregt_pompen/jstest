/* This file contains JavaScript functions to handle cookies */
/*
 * This function creates a cookie.
 * 
 * Arguments:
 * -----------
 * name:    Name of the cookie. 
 * content: Content of the cookie.
 * days:    Number of days from now before expiration.
 * hours:   Number of hours from now before expiration.
 * minutes: Number of minutes from before expiration.
 */
function setCookie(name, content, days = 0, hours = 0, minutes = 0) {
    const now = new Date();
    const exp = new Date(now.getTime() +
        (((((days * 24) + hours) * 60) + minutes) * 60000)); //verval cookies
    const expiration = exp.toUTCString();
    name = encodeURIComponent(name); //geen speciale tekens
    content = encodeURIComponent(content);
    document.cookie = `${name}=${content};expires=${expiration}`;
}
/*
 * This function gets the content of a cookie.
 * 
 * Arguments: ---------- name: Name of the cookie.
 */
function getCookie(name) {
    name = encodeURI(name);
    const regex = new RegExp(name + "=([^;]+)"); // ^alles na ...
    const result = regex.exec(document.cookie);
    if (result)
        return decodeURIComponent(result[1]);
    else
        return null;
}
/*
 * This function clears a cookie.
 * 
 * Arguments: ---------- name: Name of the cookie.
 */
function clearCookie(name) {
    setCookie(name, "", -1);
}
/* This array contains a list of text-types (including HTML5) */
const textTypes = ["text", "color", "date", "datetime", "datetime-local", "email", "month", "number", "range", "search", "tel", "time", "url", "week"];
/*
 * This function fill a form with data from a cookie. Arguments: ----------
 * form: The form to fill.
 */
function loadFormCookie(form) {
    let elements = form.getElementsByTagName("input"); // getter for elementen van cookes form.elements
    if (form == null) { console.log("form is empty") } else {
        for (let i = 0; i < elements.length; i++) {
            if (textTypes.indexOf(elements[i].type) >= 0) { // if you reach a valid input
                let value = getCookie(elements[i].name) //naam+value is req
                elements[i].value = value ? value : ""; //string ="" niet null
            }
        }
    }
}
/*
 * This function write all text-content to a cookie. Arguments: ---------- form:
 * The form
 */
function saveFormCookie(form) {
    let elements = form.getElementsByTagName("input");
    for (let i = 0; i < elements.length; i++) {
        if (textTypes.indexOf(elements[i].type) >= 0) {
            setCookie(elements[i].name, elements[i].value, 0, 1, 0);
        }
    }
}